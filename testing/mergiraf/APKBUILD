# Maintainer: omni <omni+alpine@hack.org>
maintainer="omni <omni+alpine@hack.org>"
pkgname=mergiraf
pkgver=0.2.0
pkgrel=0
pkgdesc="syntax-aware git merge driver for a growing collection of programming languages and file formats"
arch="all"
url="https://mergiraf.org/"
license="GPL-3.0-or-later"
makedepends="cargo cargo-auditable"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://codeberg.org/mergiraf/mergiraf/archive/v$pkgver.tar.gz
	Cargo.lock.patch
	"
builddir="$srcdir/$pkgname"

case "$CARCH" in
arm*|x86)
	# 54 tests fail on 32b architectures
	options="$options !check" ;;
esac

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen
}

check() {
	cargo test --frozen
}

package() {
	install -Dm0755 -t "$pkgdir"/usr/bin/ target/release/"$pkgname"


	install -Dm0644 -t "$pkgdir"/usr/share/doc/"$pkgname"/ \
		doc/src/adding-a-language.md \
		doc/src/architecture.md \
		doc/src/conflicts.md \
		doc/src/languages.md \
		doc/src/related-work.md \
		doc/src/usage.md \
		doc/src/*.svg

	install -Dm0644 -t "$pkgdir"/usr/share/doc/"$pkgname"/helpers/ \
		helpers/*.sh
	install -Dm0644 "$startdir"/helpers-README \
		"$pkgdir"/usr/share/doc/"$pkgname"/helpers/README
}

sha512sums="
88ff389b4441986430cbf4c8ee32d4cf51f7339864a323532870ef836c9f5ff51f35d8b0ecc02b20647003277e398227500b301ff589d5416821e8d6a9e10bbf  mergiraf-0.2.0.tar.gz
9dc65207d22010bf1f1916c601d0f9923f5bcded8aa6ba590f6cdf5ad0a6a4f0aedc88344a3da6c4b68a6ecd34c9727163c2a9356cf70fdc2d01629e03156ee7  Cargo.lock.patch
"
